﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rework;

namespace Rework
{
    public interface ILoad
    {
        string WhereToLoad();
        void OpenFile();
    }

    public class LoadXml : ILoad
    {
        static List<string> list = new List<string>();
        static int k = 0;

        public string WhereToLoad()
        {
            Console.WriteLine("Enter the full path of the .xml file.");
            String path = Console.ReadLine();
            return path;
        }

        public void OpenFile()
        {
            LoadXml loadXmlObj = new LoadXml();
            string path = loadXmlObj.WhereToLoad();

            while (Path.GetExtension(path) != ".xml")
            {
                Console.WriteLine("File not a .xml extension! Enter the full path of the .xml file.");
                path = Console.ReadLine();
            }

            if (Path.GetExtension(path) == ".xml")
            {
                try
                {
                    StringBuilder result = new StringBuilder();
                    //Load xml
                    XDocument xdoc = XDocument.Load(path);

                    //Run query
                    var lv1s = from lv1 in xdoc.Descendants("level1")
                               select new
                               {
                                   Header = lv1.Attribute("name").Value,
                                   Children = lv1.Descendants("level2")
                               };

                    //Loop through results
                    foreach (var lv1 in lv1s)
                    {
                        result.AppendLine(lv1.Header);
                        foreach (var lv2 in lv1.Children)
                            result.AppendLine("     " + lv2.Attribute("name").Value);
                    }
                }
                catch (IOException e)
                {
                    Console.Write(e.Message);
                }

                Console.WriteLine("Now moving to creating a contact...");
                Save.CreateContact(list, k, path);
            }
        }
    }

    public class LoadTxt : ILoad
    {
        static List<string> list = new List<string>();
        static int k = 0;

        public string WhereToLoad()
        {
            Console.WriteLine("Enter the full path of the .txt file.");
            String path = Console.ReadLine();
            return path;
        }

        public void OpenFile()
        {
            LoadTxt loadTxtObj = new LoadTxt();
            string path = loadTxtObj.WhereToLoad();

            while (Path.GetExtension(path) != ".txt")
            {
                Console.WriteLine("File not a .txt extension! Enter the .txt file name.");
                path = Console.ReadLine();
            }

            if (Path.GetExtension(path) == ".txt")
            {
                try
                {
                    using (StreamReader reader = File.OpenText(path))
                    {
                        string line;

                        while ((line = reader.ReadLine()) != null)
                        {
                            k++;
                            list.Add(line);
                        }
                        k /= 6;
                    }
                }
                catch (IOException e)
                {
                    Console.Write(e.Message);
                }

                Console.WriteLine("Now moving to creating a contact...");
                Save.CreateContact(list, k, path);
            }
        }
    }

    public class Load
    {
        public static void LoadContact()
        {
            Console.Clear();
            Console.WriteLine("Press [1] for .txt. Press [2] for .xml.");
            string result = Console.ReadLine();

            LoadXml xmlObj = new LoadXml();
            LoadTxt txtObj = new LoadTxt();

            Dictionary<string, Action> loadMenu = new Dictionary<string, Action>();

            loadMenu.Add("1", () => txtObj.OpenFile());
            loadMenu.Add("2", () => xmlObj.OpenFile());

            var replaceSwitch = new Dictionary<string, Action>
            {
                {"1", () => txtObj.OpenFile()},
                {"2", () => xmlObj.OpenFile()}
            };

            replaceSwitch[result]();
        }
    }
}
