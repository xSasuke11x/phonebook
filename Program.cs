﻿/*
 *   Author: Kennedy Tran
 *   Class: CSI - 335 - 51
 *   Assignment: 1
 *   Date Assigned: 1/30/14
 *   Due Date: 5:30 PM @ 2/6/14
 *   Description: This program is a phonebook that allows you to add contacts
 *                  and save them in a txt or xml file. You can also load up
 *                  your own contacts.
 *   Certification of Authenticity:
 *   I certify that this assignment is entirely my own work.
 *   
 * **************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Rework;

namespace Rework
{
    public class Program
    {
        static List<string> tempList = new List<string>();
        static int tempNumber = 0;
        static string tempString = "";

        static void Main(string[] args)
        {
            Dictionary<string, Action> menu = new Dictionary<string, Action>();

            Console.WriteLine("Press [1] to load contacts. Press [2] to save a new contact. Press [3] to exit.");
            string result = Console.ReadLine();

            menu.Add("1", () => Load.LoadContact());
            menu.Add("2", () => Save.CreateContact(tempList, tempNumber, tempString));
            menu.Add("3", () => Environment.Exit(0));

            var replaceSwitch = new Dictionary<string, Action>
            {
                {"1", () => Load.LoadContact()},
                {"2", () => Save.CreateContact(tempList, tempNumber, tempString)},
                {"3", () => Environment.Exit(0)}
            };

            replaceSwitch[result]();

            Console.WriteLine("Press Enter to Exit");
            Console.Read();
        }
    }
}
