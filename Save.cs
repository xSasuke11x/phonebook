﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rework
{
    public interface ISave
    {
        string WhereToSave();
        void SaveContact(PhoneBook contact, List<string> stringList, int k);
    }

    public class CheckingContacts
    {
        public bool CheckContacts(PhoneBook contact, List<string> stringList, int k)
        {
            bool safe = true;
            var temp = contact.phoneNumber.ToString();
            foreach (string s in stringList)
            {
                if (s == temp)
                {
                    safe = false;
                    return safe;
                }
            }
            return safe;
        }
    }

    public class SaveTxt : ISave
    {
        // An empty constructor
        public SaveTxt() { }

        public string WhereToSave()
        {
            Console.WriteLine("Name the file that you want to save with the extension .txt");
            string path = Console.ReadLine();

            do
            {
                if (Path.GetExtension(path) != ".txt")
                {
                    Console.WriteLine("Error! Extension is not a .txt!");
                    Console.WriteLine("Name the file that you want to save with the extension .txt");
                    path = Console.ReadLine();
                }
            } while (Path.GetExtension(path) != ".txt");

            return path;
        }

        public void SaveContact(PhoneBook contact, List<string> stringList, int k)
        {
            SaveTxt txtObj = new SaveTxt();
            string path = txtObj.WhereToSave();

            //Try/catch a double check? Useless?
            Console.WriteLine("Checking file location to see if contact already exists...");
            try
            {
                using (StreamReader reader = File.OpenText(path))
                {
                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        stringList.Add(line);
                    }
                }
            }
            catch (IOException e)
            {
                Console.Write(e.Message);
            }

            using (TextWriter writer = new StreamWriter(@path, true)) //Originally @"contacts.txt"
            {
                CheckingContacts contactsObj = new CheckingContacts();
                if (contactsObj.CheckContacts(contact, stringList, k))
                {
                    Console.WriteLine("Adding contact " + contact.name + " to the address book.");
                    writer.WriteLine(contact.name);
                    writer.WriteLine(contact.street);
                    writer.WriteLine(contact.city);
                    writer.WriteLine(contact.state);
                    writer.WriteLine(contact.zipCode);
                    writer.WriteLine(contact.phoneNumber);
                }
                else
                {
                    Console.WriteLine("Error! Contact already exists in file!");
                }
            }
        }
    }

    public class SaveXml : ISave
    {
        // An empty constructor
        public SaveXml() { }

        public string WhereToSave()
        {
            Console.WriteLine("Name the file that you want to save with the extension .xml");
            string path = Console.ReadLine();

            do
            {
                if (Path.GetExtension(path) != ".xml")
                {
                    Console.WriteLine("Error! Extension is not a .xml!");
                    Console.WriteLine("Name the file that you want to save with the extension .xml");
                    path = Console.ReadLine();
                }
            } while (Path.GetExtension(path) != ".xml");

            return path;
        }

        public void SaveContact(PhoneBook contact, List<string> stringList, int k)
        {
            CheckingContacts contactsObj = new CheckingContacts();
            SerializeOrderClass serializeObj = new SerializeOrderClass();
            DeserializeOrderClass deserializeObj = new DeserializeOrderClass();
            SaveXml xmlObj = new SaveXml();
            string path = xmlObj.WhereToSave();

            Console.WriteLine("Checking file location to see if contact already exists...");
            if (contactsObj.CheckContacts(contact, stringList, k))
            {
                Console.WriteLine("Adding contact " + contact.name + " to the address book.");
            }
            else
            {
                Console.WriteLine("Contact already exists. Now exiting!");
                Environment.Exit(0);
            }

            PhoneBook[] phoneBook = new PhoneBook[k];

            for (int j = 0; j < k; j++)
            {
                phoneBook[j] = new PhoneBook(contact.name, contact.street, contact.city, contact.state,
                    contact.zipCode, contact.phoneNumber);
            }

            var people = (from peopleQuery in stringList
                          select peopleQuery).ToArray();

            // serializing in xml
            serializeObj.SerializeOrders(phoneBook, path);

            // deserializing the xml
            var serializedOrders = deserializeObj.Deserialize(path);
        }
    }

    public class SerializeOrderClass
    {
        public void SerializeOrders(PhoneBook[] phoneBook, string path)
        {
            XmlSerializer parse = new XmlSerializer(typeof(PhoneBook[]));
            using (var writer = new StreamWriter(path))
            {
                parse.Serialize(writer, phoneBook);
                writer.Close();
            }
        }
    }

    public class DeserializeOrderClass
    {
        public PhoneBook[] Deserialize(string path)
        {
            XmlSerializer ser = new XmlSerializer(typeof(PhoneBook[]));
            PhoneBook[] result;
            using (XmlReader reader = XmlReader.Create(path))
            {
                result = (PhoneBook[])ser.Deserialize(reader);
            }
            return result;
        }
    }

    public class Save
    {
        public static void CreateContact(List<string> list, int k, string path)
        {
            List<string> stringList = list.ConvertAll(i => i.ToString());
            k++;

            PhoneBook contact = new PhoneBook();
            SaveTxt txtObj = new SaveTxt();
            SaveXml xmlObj = new SaveXml();
            String input;

            Console.WriteLine("Enter the name of the new contact.");
            contact.name = Console.ReadLine();

            Console.WriteLine("Enter the name of the street address.");
            contact.street = Console.ReadLine();

            Console.WriteLine("Enter the name of the city.");
            contact.city = Console.ReadLine();

            Console.WriteLine("Enter the state name.");
            contact.state = Console.ReadLine();

            Console.WriteLine("Enter the the zip code of the contact.");
            var temp = Console.ReadLine();
            while (temp.Length != 5)
            {
                Console.WriteLine("Error. Zipcode is not 5 digits. Please enter a valid number.");
                temp = Console.ReadLine();
            }

            contact.zipCode = int.Parse(temp);

            Console.WriteLine("Enter the 10-digit phone number of the new contact.");
            var temp2 = Console.ReadLine();
            while (temp2.Length != 10)
            {
                Console.WriteLine("Error. Phone number is not 10 digits. Please enter a valid number.");
                temp2 = Console.ReadLine();
            }

            contact.phoneNumber = long.Parse(temp2);

            input = Path.GetExtension(path);

            if (input == ".txt")
                txtObj.SaveContact(contact, stringList, k);
            else if (input == ".xml")
                xmlObj.SaveContact(contact, stringList, k);
            else
            {
                return;
            }
        }
    }

    [Serializable]
    public class PhoneBook
    {
        public String name { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public long phoneNumber { get; set; }
        public int zipCode { get; set; }

        public PhoneBook()
        {
            name = "";
            street = "";
            city = "";
            state = "";
            phoneNumber = 0000000000;
            zipCode = 00000;
        }
        public PhoneBook(string name, string street, string city, string state, int zipCode, long phoneNumber)
        {
            this.name = name;
            this.street = street;
            this.city = city;
            this.state = state;
            this.zipCode = zipCode;
            this.phoneNumber = phoneNumber;
        }
    }
}
